package stud;

import stud.entity.Book;
import stud.service.IBookService;
import stud.service.jdbc.BookServiceJDBC;
import java.io.*;
import java.util.List;
import java.util.Scanner;


/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 22.04.15
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */


public class Main {
    public static void main(String args[]) throws IOException{
        IBookService accessData = new BookServiceJDBC();

        Scanner in = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));

        Answers ans;
        List<Book> list;
        String inAnswer;

        System.out.println("Hello random visitor!");
        label:
        while (true) {
            System.out.println("Options :");
            System.out.println("1 - take all books from database");
            System.out.println("2 - take books by author");
            System.out.println("3 - take books by genre");
            System.out.println("4 - take books by part of title");
            System.out.println("5 - take books by price range");
            System.out.println("q - quit");

            inAnswer = br.readLine();

            if (inAnswer.equals("1"))
                ans = Answers.one;
            else if (inAnswer.equals("2"))
                ans = Answers.two;
            else if (inAnswer.equals("3"))
                ans = Answers.three;
            else if (inAnswer.equals("4"))
                ans = Answers.four;
            else if (inAnswer.equals("5"))
                ans = Answers.five;
            else if (inAnswer.equals("q"))
                ans = Answers.quit;
            else
                ans = Answers.inCorr;

            switch (ans) {
                case one:
                    System.out.println("All books from database :");
                    list = accessData.getAllBooks();
                    for (Book book : list)
                        System.out.println(book);
                    if (list.size() == 0)
                        System.out.println("Books not found");
                    System.out.println();
                    break;

                case two:
                    System.out.print("Enter author name : ");
                    String authorName = in.nextLine();
                    list = accessData.getBooksByAuthor(authorName);
                    for (Book book : list)
                        System.out.println(book);
                    if (list.size() == 0)
                        System.out.println("Books not found");
                    System.out.println();
                    break;

                case three:
                    System.out.print("Enter genre : ");
                    String genre = in.nextLine();
                    list = accessData.getBooksByGenre(genre);
                    for (Book book : list)
                        System.out.println(book);
                    if (list.size() == 0)
                        System.out.println("Books not found");
                    System.out.println();
                    break;

                case four:
                    System.out.print("Enter part of title : ");
                    String partOfTitle = in.nextLine();
                    list = accessData.getBooksByPartOfTitle(partOfTitle);
                    for (Book book : list)
                        System.out.println(book);
                    if (list.size() == 0)
                        System.out.println("Books not found");
                    System.out.println();
                    break;

                case five:
                    System.out.print("Enter min price : ");
                    double minPrice = in.nextDouble();

                    if (minPrice < 0) {
                        System.out.println("Incorrect min price, try again");
                        System.out.println();
                        break;
                    }
                    System.out.print("Enter max price : ");
                    double maxPrice = in.nextDouble();
                    if (maxPrice < 0) {
                        System.out.println("Incorrect max price, try again");
                        System.out.println();
                        break;
                    }

                    list = accessData.getBooksByPriceRange(minPrice, maxPrice);
                    for (Book book : list)
                        System.out.println(book);
                    if (list.size() == 0)
                        System.out.println("Books not found");
                    System.out.println();
                    break ;

                case quit:                    
                    break label;
                default:
                    System.out.println("Incorrect statement, try again");
                    System.out.println();
                    break;
            }
        }
        System.out.println("Good luck!");
    }
}
