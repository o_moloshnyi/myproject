package stud.entity;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 23.04.15
 * Time: 11:10
 * To change this template use File | Settings | File Templates.
 */
public class Book {
    private long id;
    private String name;
    private String author;
    private String genre;
    private int numberPages;
    private double price;
    private boolean translated;

    public Book(String name , String author, String genre, int numberPages, double price, boolean translated) {
        this.name = name;
        this.author = author;
        this.genre = genre;
        this.numberPages = numberPages;
        this.price = price;
        this.translated = translated;
    }

    public Book() {
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getGenre() {
        return genre;
    }

    public void setNumberPages (int numberPages) {
        this.numberPages = numberPages;
    }

    public int getNumberPages() {
        return numberPages;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setTranslated(boolean translated) {
        this.translated = translated;
    }

    public boolean getTranslated() {
        return translated;
    }

    public String toString() {
        return id + " " + name + " " + author + " " + genre +
                " " + numberPages + " " + price + " " + translated;
    }


}
