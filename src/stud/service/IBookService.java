package stud.service; /**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 23.04.15
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */

import stud.entity.Book;

import java.util.*;

public interface IBookService {

    List<Book> getAllBooks();

    List<Book> getBooksByAuthor(String authorName);

    List<Book> getBooksByGenre(String genre);

    List<Book> getBooksByPriceRange(Double minPrice , Double maxPrice);

    List<Book> getBooksByPartOfTitle(String partOfTitle);

    void addBook(Book newBook);

    void deleteLastBook();

    void setAuthorNameById(int id, String newName);

    void updateBook(int id, Book book);

    void deleteBookById(int id);
}


