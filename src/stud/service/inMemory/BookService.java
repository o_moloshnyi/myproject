package stud.service.inMemory;

import stud.entity.Book;
import stud.service.IBookService;
import stud.utilit.BookDataBase;
import sun.java2d.loops.GeneralRenderer;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 23.04.15
 * Time: 12:37
 * To change this template use File | Settings | File Templates.
 */
public class BookService implements IBookService {

    public List<Book> getAllBooks() {
        return BookDataBase.getBookListDataBase();
    }

     public List<Book> getBooksByAuthor(String authorName){
        List<Book> localBookList = new ArrayList<Book>();
        for (Book book : BookDataBase.getBookListDataBase()) {
            if (book.getAuthor().equals(authorName))
                localBookList.add(book);
        }
        return  localBookList;
    }

    public void addBook(Book newBook) {
        BookDataBase.getBookListDataBase().add(newBook);
    }

    public void deleteLastBook() {
        BookDataBase.getBookListDataBase().remove(BookDataBase.getBookListDataBase().size() - 1);
    }

    public void setAuthorNameById(int index, String newName) {
        BookDataBase.getBookListDataBase().get(index).setAuthor(newName);
    }

    @Override
    public void updateBook(int id, Book book) {

    }

    @Override
    public void deleteBookById(int id) {

    }

    @Override
    public List<Book> getBooksByGenre(String genre) {
        return null;
    }

    @Override
    public List<Book> getBooksByPriceRange(Double minPrice , Double maxPrice){
        return null;
    }

    @Override
    public List<Book> getBooksByPartOfTitle(String partOfTitle){
        return null;
    }

}
