package stud.service.jdbc;

import stud.entity.Book;
import stud.service.IBookService;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;
/**
 * Created with IntelliJ IDEA.
 * User: Oleksandr
 * Date: 10.05.15
 * Time: 10:17
 * To change this template use File | Settings | File Templates.
 */
public class BookServiceJDBC implements IBookService {

    @Override
    public List<Book> getAllBooks() {
        String selecetDataSQL = "SELECT * FROM stud_db.bookdatabase;";
        List<Book> localListBook = new ArrayList<Book>();
        Connection connection = getDBConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(selecetDataSQL);
            while (rs.next()) {
                Book book = new Book();
                book.setId(rs.getLong("id"));
                book.setName(rs.getString("name"));
                book.setAuthor(rs.getString("author"));
                book.setGenre(rs.getString("genre"));
                book.setNumberPages(rs.getInt("numberPages"));
                book.setPrice(rs.getDouble("price"));
                book.setTranslated(rs.getBoolean("translated"));
                localListBook.add(book);
            }
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return localListBook;
    }

    @Override
    public List<Book> getBooksByAuthor(String authorName) {
        String selecetDataSQL = "SELECT * FROM stud_db.bookdatabase;";
        List<Book> localListBook = new ArrayList<Book>();
        Connection connection = getDBConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selecetDataSQL);
            while (resultSet.next()) {
                if (resultSet.getString("author").equals(authorName)){
                    Book book = new Book();
                    book.setId(resultSet.getLong("id"));
                    book.setName(resultSet.getString("name"));
                    book.setAuthor(resultSet.getString("author"));
                    book.setGenre(resultSet.getString("genre"));
                    book.setNumberPages(resultSet.getInt("numberPages"));
                    book.setPrice(resultSet.getDouble("price"));
                    book.setTranslated(resultSet.getBoolean("translated"));
                    localListBook.add(book);
                }
            }
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return localListBook;
    }

    @Override
    public List<Book> getBooksByGenre(String genre) {
        String selecetDataSQL = "SELECT * FROM stud_db.bookdatabase;";
        List<Book> localListBook = new ArrayList<Book>();
        Connection connection = getDBConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selecetDataSQL);
            while (resultSet.next()) {
                if (resultSet.getString("genre").equals(genre)){
                    Book book = new Book();
                    book.setId(resultSet.getLong("id"));
                    book.setName(resultSet.getString("name"));
                    book.setAuthor(resultSet.getString("author"));
                    book.setGenre(resultSet.getString("genre"));
                    book.setNumberPages(resultSet.getInt("numberPages"));
                    book.setPrice(resultSet.getDouble("price"));
                    book.setTranslated(resultSet.getBoolean("translated"));
                    localListBook.add(book);
                }
            }
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return localListBook;
    }

    @Override
    public List<Book> getBooksByPriceRange(Double minPrice , Double maxPrice){
        String selecetDataSQL = "SELECT * FROM stud_db.bookdatabase;";
        List<Book> localListBook = new ArrayList<Book>();
        Connection connection = getDBConnection();
        Statement statement = null;

        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selecetDataSQL);
            while (resultSet.next()) {
                if ((resultSet.getDouble("price")) > minPrice &&
                        (resultSet.getDouble("price") < maxPrice)){
                    Book book = new Book();
                    book.setId(resultSet.getLong("id"));
                    book.setName(resultSet.getString("name"));
                    book.setAuthor(resultSet.getString("author"));
                    book.setGenre(resultSet.getString("genre"));
                    book.setNumberPages(resultSet.getInt("numberPages"));
                    book.setPrice(resultSet.getDouble("price"));
                    book.setTranslated(resultSet.getBoolean("translated"));
                    localListBook.add(book);
                }
            }
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return localListBook;
    }

    @Override
    public List<Book> getBooksByPartOfTitle(String partOfTitle){
        String selecetDataSQL = "SELECT * FROM stud_db.bookdatabase;";
        List<Book> localListBook = new ArrayList<Book>();
        Connection connection = getDBConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selecetDataSQL);
            while (resultSet.next()) {
                if (resultSet.getString("name").contains(partOfTitle)){
                    Book book = new Book();
                    book.setId(resultSet.getLong("id"));
                    book.setName(resultSet.getString("name"));
                    book.setAuthor(resultSet.getString("author"));
                    book.setGenre(resultSet.getString("genre"));
                    book.setNumberPages(resultSet.getInt("numberPages"));
                    book.setPrice(resultSet.getDouble("price"));
                    book.setTranslated(resultSet.getBoolean("translated"));
                    localListBook.add(book);
                }
            }
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return localListBook;
    }

    @Override
    public void addBook(Book newBook) {
        String insertDataSQL = "INSERT INTO stud_db.bookdatabase" +
                               "(id, name, author, genre, numberPages, price, translated) VALUES " +
                               "(NULL ,'" + newBook.getName() + "','" + newBook.getAuthor() + "','" +
                                newBook.getGenre() + "'," + newBook.getNumberPages() + ","+ newBook.getPrice() + "," +
                                newBook.getTranslated() +");";

        Connection connection = getDBConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(insertDataSQL);
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public void deleteLastBook() {
        String selecetDataSQL = "SELECT id FROM stud_db.bookdatabase;";
        Connection connection = getDBConnection();
        Statement statement = null;
        int maxId = 0;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selecetDataSQL);
            while (resultSet.next()) {
                if (resultSet.getInt("id") > maxId) maxId = resultSet.getInt("id");
            }
            String deleteDataSQL = "DELETE FROM stud_db.bookdatabase WHERE stud_db.bookdatabase.id =" + maxId + ";";
            statement.execute(deleteDataSQL);
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public void setAuthorNameById(int id, String newAuthorName) {
        String selecetDataSQL = "SELECT id FROM stud_db.bookdatabase;";
        String updateTableSQL = "UPDATE stud_db.bookdatabase SET stud_db.bookdatabase.author = '" +
                                 newAuthorName + "' WHERE id = " + id + ";";

        Connection connection = getDBConnection();
        Statement statement = null;
        boolean confirmation = false;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selecetDataSQL);
            while (resultSet.next()) {
                if (resultSet.getInt("id") == id) confirmation = true;
            }
            if (confirmation)
                statement.execute(updateTableSQL);
            else
                System.out.println("stud.entity.Book with this id doesn`t exist");
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public void updateBook(int id, Book book) {
        String selecetDataSQL = "SELECT id FROM stud_db.bookdatabase;";

        String updateTableSQL = "UPDATE stud_db.bookdatabase SET " +
                                " stud_db.bookdatabase.name = '" +book.getName() +
                                "',stud_db.bookdatabase.author = '" + book.getAuthor() +
                                "',stud_db.bookdatabase.genre = '" + book.getGenre() +
                                "',stud_db.bookdatabase.numberPages = " + book.getNumberPages() +
                                ", stud_db.bookdatabase.price = " + book.getPrice() +
                                ", stud_db.bookdatabase.translated = " + book.getTranslated() +
                                " WHERE id = " + id + ";";

        Connection connection = getDBConnection();
        Statement statement = null;
        boolean confirmation = false;
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selecetDataSQL);
            while (resultSet.next()) {
                if (resultSet.getInt("id") == id) confirmation = true;
            }
            if (confirmation)
                statement.execute(updateTableSQL);
            else
                System.out.println("stud.entity.Book with this id doesn`t exist");
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public void deleteBookById(int id) {
        String deleteDataSQL = "DELETE FROM stud_db.bookdatabase WHERE stud_db.bookdatabase.id =" + id + ";";
        Connection connection = getDBConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.execute(deleteDataSQL);
            connection.close();
            statement.close();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }

    private Connection getDBConnection() {
        String dbDriver =  "com.mysql.jdbc.Driver";
        String dbConnection =  "jdbc:mysql://localhost:3306";
        String dbUser = "root";
        String dbPassword = "";

        Connection connection = null;
        try {
            Class.forName(dbDriver);
        }
        catch (ClassNotFoundException e) {
            System.out.println(e);
        }
        try {
            connection = DriverManager.getConnection(dbConnection, dbUser,dbPassword);
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return connection;
    }
}
